package com.hepta.mercado.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.jupiter.api.Test;

import com.hepta.mercado.entity.Fabricante;
import com.hepta.mercado.entity.Produto;
import com.hepta.mercado.persistence.FabricanteDAO;
import com.hepta.mercado.persistence.ProdutoDAO;

public class DaoTest {
	
	FabricanteDAO fDao = new FabricanteDAO();
	ProdutoDAO pDao = new ProdutoDAO();
	
	
	@Test
	public void save() {
		Fabricante fab1 = new Fabricante();
		fab1.setNome("Tio Jo�o");
		Fabricante fab2 = new Fabricante();
		fab1.setNome("Camil");
		
		Produto prod = new Produto();
		prod.setFabricante(fab1);
		prod.setNome("Arroz");
		prod.setUnidade("kg");
		prod.setVolume(5.0);
		prod.setEstoque(10);
		
		try {
			fDao.save(fab1);
			fDao.save(fab2);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		try {
			pDao.save(prod);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	@Test
	public void find() {
		
		Fabricante fab = new Fabricante();
		Produto prod = new Produto();
		
		try {
		fab = fDao.find(1);
		System.out.println(fab.getNome());
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		try {
		prod = pDao.find(1);
		System.out.println(prod.getNome());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	@Test
	public void update() throws Exception {
		Fabricante fab = new Fabricante();
		Produto prod = new Produto();

		try {
		fab = fDao.find(2);
		prod = pDao.find(1);
		
		prod.setFabricante(fab);
		prod.setNome("Arroz Integral");
		
		pDao.update(prod);
		System.out.println("Fabricante: " + prod.getFabricante().getNome() +"\nNome: " +prod.getNome());
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	@Test
	public void delete() {
		
		try {
		fDao.delete(1);
		pDao.delete(1);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	
	@Test
	public void getAll() {
		List<Fabricante> fabricantes = new ArrayList<>();
		List<Produto> produtos = new ArrayList<>();
		
		try {
		fabricantes = fDao.getAll();
		produtos = pDao.getAll();
		
		System.out.println("Fabricantes: " + fabricantes.size()+"\nProdutos: " + produtos.size());
		
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}
}
